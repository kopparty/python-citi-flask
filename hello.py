from flask import Flask
from flask import request
from cda import Cda
from query_params_mongo import QueryParams
from pymongo import MongoClient
from sshtunnel import SSHTunnelForwarder
import yaml

app = Flask(__name__)


@app.route('/')
def hello_world():
    return 'Hello, World!'


@app.route('/speip', methods=['POST'])
def speip():
    if request.method == 'POST' and request.is_json:
        try:
            datos_cda = request.json
            cda_enviado = build_cda(datos_cda)

            return cda_enviado.imprimir_cda()
        except Exception as exception:
            return str(exception)


@app.route('/getspeip', methods=['POST'])
def get_speip():
    config = get_config()
    if request.method == 'POST' and request.is_json:
        try:
            datos_params = request.json
            query_params = build_query_parameters(datos_params)

            if bool(config.get('usessh')):
                with SSHTunnelForwarder(
                        (config.get('host')),
                        ssh_username=config.get('user'),
                        ssh_pkey=config.get('sshkey'),
                        remote_bind_address=(config.get('remoteaddress'), int(config.get('port')))
                ) as tunnel:
                    print("****SSH Tunnel Established****")
                    client = MongoClient(config.get('remoteaddress'), tunnel.local_bind_port)
                    with client:
                        query_mongodb(client, config, query_params)

            else:
                client = MongoClient('mongodb://{host}:{port}/'.format(
                    host=config.get('host'),
                    port=config.get('port')
                ))

                with client:
                    query_mongodb(client, config, query_params)

        except Exception as exception:
            return str(exception)


def query_mongodb(client, config, query_params):
    db = client[config.get('database')]
    collection = db[config.get('collection')]
    consulta = build_query(query_params)
    resultados = collection.find_one(consulta)

    if resultados:
        return resultados
    else:
        return 'No se encontraron registros'


def build_query(query_params):
    consulta = {}

    if query_params.clave_rastreo:
        consulta["customerHubModel.trackingKey"] = query_params.clave_rastreo
    if query_params.rfc_curp:
        consulta["customerHubModel.rFCOCurpBeneficiario"] = query_params.rfc_curp
    return consulta


def build_cda(datos_cda):
    cda_enviado = Cda(datos_cda['Clave de Rastreo'],
                      datos_cda['Tipo de pago'],
                      datos_cda['Fecha de operación del SPEI'],
                      datos_cda['Fecha calendario del abono'],
                      datos_cda['Hora calendario del abono'],
                      datos_cda['Clave SPEI del Participante Emisor de la CDA'],
                      datos_cda['Nombre del Participante Emisor de la Orden de Transferencia'],
                      datos_cda['Nombre Ordenante'],
                      datos_cda['Tipo de Cuenta Ordenante'],
                      datos_cda['Cuenta Ordenante'],
                      datos_cda['RFC o CURP Ordenante'],
                      datos_cda['Tipo de Cuenta Beneficiario'],
                      datos_cda['Concepto del Pago'],
                      datos_cda['Importe del IVA'],
                      datos_cda['Tipo de Cuenta Beneficiario 2'],
                      datos_cda['Folio del Esquema Cobro Digital'],
                      datos_cda['Pago de la comisión por la transferencia'],
                      datos_cda['Monto de la comisión por la transferencia'],
                      datos_cda['Alias del número celular del comprador'],
                      datos_cda['Digito verificador del comprador'])
    return cda_enviado


def get_config():
    with open("config.yml", 'r') as yml_file:
        config = yaml.load(yml_file, Loader=yaml.FullLoader)
        return config['mongodb']


def build_query_parameters(datos_query):
    query_params = QueryParams(datos_query['claveRastreo'] if 'claveRastreo' in datos_query else '',
                               datos_query["rfcCurp"] if 'rfcCurp' in datos_query else None)

    return query_params
