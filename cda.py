class Cda:
    def __init__(self, clave_de_rastreo, tipo_de_pago, fecha_operacion_spei, hora_calendario_abono, clave_spei_emisor,
                 nombre_emisor, nombre_ordenante, tipo_cuenta_ordenante,
                 cuenta_ordenante, rfc_curp_ordenante, tipo_cuenta_beneficiario, concepto_pago, importe_iva, monto_pago,
                 tipo_cuenta_beneficiario_2, folio_esquema, pago_comision_transferencia, monto_comision_transferencia,
                 alias_celular_comprador, digito_verificador_comprador):
        self.clave_de_rastreo = clave_de_rastreo
        self.tipo_de_pago = tipo_de_pago
        self.fecha_operacion_spei = fecha_operacion_spei
        self.hora_calendario_abono = hora_calendario_abono
        self.clave_spei_emisor = clave_spei_emisor
        self.nombre_emisor = nombre_emisor
        self.nombre_ordenante = nombre_ordenante
        self.tipo_cuenta_ordenante = tipo_cuenta_ordenante
        self.cuenta_ordenante = cuenta_ordenante
        self.rfc_curp_beneficiario = rfc_curp_ordenante
        self.tipo_cuenta_beneficiario = tipo_cuenta_beneficiario
        self.concepto_pago = concepto_pago
        self.importe_iva = importe_iva
        self.monto_pago = monto_pago
        self.tipo_cuenta_beneficiario_2 = tipo_cuenta_beneficiario_2
        self.folio_esquema = folio_esquema
        self.pago_comision_transferencia = pago_comision_transferencia
        self.monto_comision_transferencia = monto_comision_transferencia
        self.alias_celular_comprador = alias_celular_comprador
        self.digito_verificador_comprador = digito_verificador_comprador

    @property
    def clave_de_rastreo(self):
        return self._clave_de_rastreo

    @clave_de_rastreo.setter
    def clave_de_rastreo(self, clave):
        if not clave:
            raise Exception("|| Clave de rastreo no puede estar vacío ||")
        self._clave_de_rastreo = clave

    @property
    def tipo_de_pago(self):
        return self._tipo_de_pago

    @tipo_de_pago.setter
    def tipo_de_pago(self, tipo_de_pago):
        if not tipo_de_pago:
            raise Exception("|| Tipo de pago no puede estar vacío ||")
        self._tipo_de_pago = tipo_de_pago

    def imprimir_cda(self):
        cda = '||{clave_de_rastreo}|{tipo_de_pago}|{fecha_operacion_spei}|{hora_calendario_abono}|{clave_spei_emisor}|{nombre_emisor}|{tipo_cuenta_ordenante}|{cuenta_ordenante}|{rfc_curp_beneficiario}|{tipo_cuenta_beneficiario}|{concepto_pago}|{importe_iva}|{monto_pago}|{tipo_cuenta_beneficiario_2}|{folio_esquema}|{pago_comision_transferencia}|{monto_comision_transferencia}|{alias_celular_comprador}|{digito_verificador_comprador}||'
        return cda.format(clave_de_rastreo=self.clave_de_rastreo,
                          tipo_de_pago=self.tipo_de_pago,
                          fecha_operacion_spei=self.fecha_operacion_spei,
                          hora_calendario_abono=self.hora_calendario_abono,
                          clave_spei_emisor=self.clave_spei_emisor,
                          nombre_emisor=self.nombre_emisor,
                          nombre_ordenante=self.nombre_ordenante,
                          tipo_cuenta_ordenante=self.tipo_cuenta_ordenante,
                          cuenta_ordenante=self.cuenta_ordenante,
                          rfc_curp_beneficiario=self.rfc_curp_beneficiario,
                          tipo_cuenta_beneficiario=self.tipo_cuenta_beneficiario,
                          concepto_pago=self.concepto_pago,
                          importe_iva=self.importe_iva,
                          monto_pago=self.monto_pago,
                          tipo_cuenta_beneficiario_2=self.tipo_cuenta_beneficiario_2,
                          folio_esquema=self.folio_esquema,
                          pago_comision_transferencia=self.pago_comision_transferencia,
                          monto_comision_transferencia=self.monto_comision_transferencia,
                          alias_celular_comprador=self.alias_celular_comprador,
                          digito_verificador_comprador=self.digito_verificador_comprador)
