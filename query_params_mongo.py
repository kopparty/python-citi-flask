class QueryParams:
    def __init__(self, clave_rastreo='', rfc_curp=None):
        self.clave_rastreo = clave_rastreo
        self.rfc_curp = rfc_curp

    @property
    def clave_rastreo(self):
        return self._clave_rastreo

    @clave_rastreo.setter
    def clave_rastreo(self, clave):
        if not clave:
            raise Exception("|| Clave de rastreo no puede estar vacío ||")
        self._clave_rastreo = clave
