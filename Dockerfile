FROM ubuntu:16.04

RUN apt-get update -y && \
    apt-get install -y python-pip python3-dev

# We copy just the requirements.txt first to leverage Docker cache
COPY ./requirements.txt /app/requirements.txt
COPY ./run.sh /app/run.sh
#RUN chmod +x /app/run.sh

WORKDIR /app
#RUN pip install flask
RUN pip install -r requirements.txt

COPY . /app

ENTRYPOINT [ "bash", "run.sh" ]

#CMD [ "/app/run.sh" ]
#CMD [ "cda.py" ]
